# simple-mvc-2


### O QUE É O APP E PORQUE FOI FEITO ASSIM

 Já faz um tempo que venho notando que muitos programadores front end que utilizam frameworks famosos como angular, react e vue desenvolver aplicações ricas e complexas apenas com javascript.

 Eu desenvolvi essa estrutura mvc e pretendo melhora-lá no futuro  para provar que é possível fazer aplicações profissionais sem os framworks citados acima ou outros, e ainda assim ser competitivo e produtivo. Veja bem, não tenho nada contra usar esses frameworks.

 Tem muitas coisas que precisam melhorar e outras que preciso criar para desenvolver um app e colocar em produção com essa estrutura mvc.


 Nesse teste da TORO achei uma oportunidade íncrivel para testar meus conhecimentos e a própria esturura mvc e por isso desenvolvi todo o teste com ela.

 **Essa estrutura mvc disponibiliza os seguintes recursos:**

 1.  Método para criação de componentes e templates.
 2.  Um roteador simples para troca de contexto.
 3.  Criação de templates html baseados em literal templates.
 4.  Criação de eventos customizados.
 5.  Oneway data binding - Toda alteração feita no model de um componente é refletida no template.
 6.  Comunicação entre componentes realizada por eventos.
 7.  Diretivas de construção. Essas diretivas são aplicadas no template e executada em tempo de construçaão.
 8.  Controller. O controller é um mecanismo para manipulação do DOM e chamada de métodos do MODEL. Ele tem acesso a um contexto que. disponibiliza o template e o model do componente, para facilitar e melhorar a escrita de modo geral e descomplicar o que poderia ser. complicado.
 9.  Modulos. Os módulos são objetos literais que agrupam componentes a fim de criar uma forma organizada de importá-los dentro do core da aplicação.
 10. - Uma classe App para controlar a inicialização da aplicação e centralizar seu comportamento.


 Todos os recursos citados acima e mais alguns foram utilizados para a construção do app (stocks) 
 proposto no teste que a TORO me enviou.


**Avaliem com carinho o código escrito, porque sei que poderia ser melhor, mas, acredito que por
 hora e para a finalidade do teste está compatível.**

 Em caso de **dúvidas** meu whatsApp: **11 96189-7060** não exite em entrar em contato.

 Por fim, abaixo seguem as instruções do que é preciso para executar a aplicação.



#### INFORMAÇÕES PARA EXECUTAR O APP

1.  Servidor estático
2.  Navegadores
3.  Extensões do vscode
4.  Executar o APP

#### 1 - Servidor estático

 Como o teste visava construir um app que fosse capaz de obter um json de uma suposta API e a TORO me enviou um arquivo json para fazer de conta que era a API, não utilizei um servidor construído por mim mesmo, por isso, você pode
 instalar o http-server que ele fará esse papel.

 **npm install http-server -g**

#### 2 - Navegadores 

Nos testes que realizei, o app é executado sem nenhum problema,
sem warnings ou erros.
O css também está ok.

Eu utilizei nos testes:

* Chrome versão 68
* Firefox developer 63
* Edge 42
  
Se você utilizar esses browsers acredito que não terá problema.

#### 3 - Extensões do vscode

 Como utilizei template literal para gerar os templates dos componentes,
 utilizei as extenções { lit-html } e { es6-string-html } para formatar
 o html dos templates. Por isso, posso apenas colocar /*html*/ na frente 
 do construtor do template e todo o html é formatado.

#### 4 - EXECUTAR O APP

 comando: NPM START
 Para executar o app basta abrir o terminal e digitar: npm start