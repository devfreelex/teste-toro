import Component from '../../core/component.class.js';
import loginTemplate from './login.template.js';

const loginComponent = new Component({
    name:'login-component',
    template:loginTemplate,
    routed: false,
    model:[],
    controller: (model, template) => {

        const trigger = document.querySelector('body');

        const showLogin = () => {
            trigger.addEventListener('showLogin', (e) => {
                template.classList.add('is--visible')
                slideIn();
            })
        }

        const hideLogin = () => {
            const cancel = template.querySelector('.login__cancel')
            const container = template.querySelector('.login__container');
            const content = template.querySelector('.login__content');

            cancel.addEventListener('click', (e) => {                
                container.classList.remove('is--visible');
                content.classList.remove('is--visible');

                setTimeout(() => {
                    template.classList.remove('is--visible');
                },500)
            })
        }

        const slideIn = () => {
            const container = template.querySelector('.login__container');
            const content = template.querySelector('.login__content');

            setTimeout(() => {
                container.classList.add('is--visible')
            },100)

            setTimeout(() => {
                content.classList.add('is--visible')
            },300)
        }

        showLogin();
        hideLogin();

    }
});


export default loginComponent