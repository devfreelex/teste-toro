const loginTemplate = (model) => {
    return /*html*/`
    
    <div class="login__cancel"></div>
    <div class="login__container">
        <div class="login__content">
            <form class="form" autocomplete="off">
                <label for="email" class="form__title">Informe seu email e senha</label>
                <label for="email" class="form__label">
                <input type="text" id="email" class="form__input"/>
                <span class="form__input__title">Email</span>
                </label>
                <label for="password" class="form__label">
                <input type="password" id="password" class="form__input"/>
                <span class="form__input__title">Senha</span>
                </label>
                <button class="form__action"> Login </button>
            </form>
        </div>
    </div>    
    
    `;
}

export default loginTemplate