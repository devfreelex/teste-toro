import { action } from '../../directives/action.directive.js';

const modalTemplate = (model) => {
    return /*html*/`
 <div class="modal">
    <div class="modal__container">
        <div class="modal__title">Tem certeza que deseja vender essa ação?</div>
        <div class="modal__actions">
            <a href="javascript:void(0)" class="modal__action reject" ${action('.reject', 'store', 0)}>Não</a>
            <a href="javascript:void(0)" class="modal__action accept" ${action('.accept', 'store', 1)}>Sim</a>
        </div>
    </div>
 </div>
 `;
}



export default modalTemplate