import { Emitter } from '../../core/events.class.js';
import Component from '../../core/component.class.js';

import templateModal from './modal.template.js';

const modalComponent = new Component({
    name:'modal-component',
    template: templateModal,
    routed:false,
    model:{},
    controller: (model, template) => {

        const trigger = document.querySelector('body');

        const showModal = () => {
            trigger.addEventListener('quotation', (e) => { 
                template.classList.add('is--visible')
            })
        }

        const hideModal = () => {
            trigger.addEventListener('store', (e) => { 
                template.classList.remove('is--visible')
            })
        }

        const showLogin = () => {
            const showLogin = new Emitter('showLogin', 'modal-component', {})
            trigger.addEventListener('store', (e) => {
                if (e.detail === 1) {
                    showLogin.emit()
                }
            })
        }

        showModal();
        hideModal();
        showLogin();
    }
});

export default modalComponent