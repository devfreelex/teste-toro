import Component from '../../core/component.class.js';
import { $$ } from '../../core/events.class.js';

import searchTemplate from './search.template.js';


const searchComponent = new Component({
    name: 'search-component',
    template: searchTemplate,
    model: { data: [] },
    isRouted: false,
    controller: (model, template) => {


        const getData = () => {
            return fetch('/api/stocks.json')
                .then(response => {
                    if (response.status == 200) {
                        return response.json()
                    }
                })
        }


        const find = (term, data) => {
            const reg = new RegExp(term, 'ig');
            let result = [];

            result = data.filter(value => {
                if (reg.test(value.name)) return value
            })

            return result;
        }


        $$('body', 'onkeypress', (e) => {

            if (e.target.value.length < 3) return;

            getData()
                .then(data => {

                    if (!data) return;

                    const dataModel = data.market.company;
                    const term = e.detail;
                    const result = find(term, dataModel)
                    let elem = '';


                    if (result) {
                        model.data = result;
                        elem = document.querySelector('#search');
                        elem.value = term;
                        elem.classList.add('has--value')
                        elem.focus()
                    };
                })
                .catch(err => console.log(err))
        })

    }
})

export default searchComponent