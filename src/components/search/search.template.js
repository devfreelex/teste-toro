import { keypress } from '../../directives/keypress.directive.js';
import { onblur } from '../../directives/onblur.directive.js';
import { hasValue } from '../../directives/hasValue.directive.js';
import { setClass } from '../../directives/setClass.directive.js';
import { animate } from '../../directives/animate.directive.js';
import { getCompany } from '../../directives/getCompany.js';


const searchTemplate = (model) => { 
    setTimeout( () => animate('result__link', 'has--value', model.data), 100)
      return /*html*/`
        <div class="search">
            <input id="search" class="search__input"  autocomplete="off" ${keypress('#search')} ${onblur('#search')}/>
            <span class="search__label"> Pesquisar</span>
            <button class="search__button">
                <i class="search__icon"></i>
            </button>
            <ul class="result__list ${setClass('result__list', 'has--value',  hasValue(model))}">
                ${repeat(model)}
            </ul>
        </div>
    `;   
}


const repeat = (model) => { 

    const {data} = model; 
    animate('result__link', 'has--value', data)

    if (data) {
        return data.map( result => {
            return /*html*/`
                <li class="result__item">
                    <a href="javascript:void(0)" class="result__link" ${getCompany('.result__link')}>${result.name}</a>
                </li>
            `;
        }).join('');
    }

}

export default searchTemplate

