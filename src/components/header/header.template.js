
const headerTemplate = (data) => {
    return /*html*/`
        <h1 class="grid grid-size-1 logo">${data.title}</h1>
    `;
}

export default headerTemplate