import Component from '../../core/component.class.js';
import headerTemplate from './header.template.js';


const headerComponent = new Component({
    name:'header-component',
    template:headerTemplate,
    isRouted: false,
    model: {title:'stocks.'},
    controller: (model, template) => {
        
    }
})

export default headerComponent