import { action } from '../../directives/action.directive.js';

const resultTemplate = (model) => { 

    if (!model.data || !model.data.length) { 
        return /*html*/`
        <div class="message__search">
            <i class="icon__message"></i>
            <p class="text__message">Você ainda não realizou nenhuma busca!</p>
        </div>
    `;
    }


    return /*html*/`
    <section class="quotation__container">
        <h1 class="quotation__container__title">Resultado da busca</h1>
        ${model.data.map(company => {
            const name = company.name;
            const logo = company.logo;
            const stocks = company.stock;        


           return stocks.map( stock => {
                return /*html*/`
                <article class="quotation__results">

                <header class="quotation__header">
                    <div class="quotation__img">
                        <img src="${logo}" async defer/>
                    </div>
                    <h2 class="quotation__title"> ${name} - <span class="label">${stock.name}</span></h2>
                    <span class="quotation__controll">Ocultar detalhe da ação <i class="quotation__icon"></i></span>            
                </header>
                
                <ul class="quotation__description">
                    <li class="quotation__item quotation__open"><span class="label">Abertura:</span> ${stock.openingvalue}</li>
                    <li class="quotation__item quotation__present"><span class="label">Atual:</span> ${stock.actualvalue}</li>
                    <li class="quotation__item">
                        <a href="javascript:void(0)" class="quotation__action" ${action('.quotation__action', 'quotation')}>${setText(stock.openingvalue, stock.actualvalue)}</a>
                    </li>
                </ul>
    
            </article>
                
                `;
            })


        }).join('')}
    </section>
    `;
}

const setText = (openingvalue, actualvalue) => {
    if (openingvalue < actualvalue) return 'comprar';
    return 'vender';
}


export default resultTemplate