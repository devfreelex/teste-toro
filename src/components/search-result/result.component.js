import Component from '../../core/component.class.js';
import { toggleDirective } from '../../directives/toggle.directive.js';

import resultTemplate from './result.template.js';


const resultComponent = new Component({
    name: 'result-component',
    template: resultTemplate,
    model:[{titulo:'1'}],
    isRouted: false,
    controller: (model, template) => {    

        const trigger = document.querySelector('body');

        const getData = () => {
            return fetch('/api/stocks.json')
                .then(response => {
                    if (response.status == 200) {
                        return response.json()
                    }
                })
        }

        const find = (term, data) => {
            const reg = new RegExp(term, 'ig');
            let result = [];

            result = data.filter(value => {
                if (reg.test(value.name)) return value
            })

            return result;
        }


        trigger.addEventListener('getcompany', (e) => {
            const term = e.detail;

            getData()
            .then(response => {
                const data = response.market.company;
                model.data = find(term, data)
                toggleDirective(template);
            })
            .catch( err => console.log(err))
            
        })

    }
});

export default resultComponent