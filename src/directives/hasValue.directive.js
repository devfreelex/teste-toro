export const hasValue = (model) => {
    const {data} = model;
    if (data && data.length > 0) return true;
}