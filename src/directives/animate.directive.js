export const animate = (element, strClass, data) => {
    const links = document.querySelectorAll(`.${element}`)    
    let index = 0;

    const interval = setInterval( () => {
        if (links[index]) links[index].classList.add(`${strClass}`)
        index = index + 1;
    },100)

    if(data) {
        setTimeout( () => {clearInterval(interval)}, (data.length * 100))
    }
   
}