export const toggleDirective = (template) => {

    const elem = document.querySelectorAll('.quotation__results');
    const container = document.querySelector('.quotation__container')

    const toggleComponent = () => {

        setTimeout(() => {
            container.classList.add('is--toped')
        }, 100)

        setTimeout(() => {
            container.classList.add('is--visible')
        }, 300)
    
        elem.forEach(element => {    
            element.addEventListener('click', (e) => {
                const detail = e.target.parentNode.nextElementSibling;
                const icon = e.target.querySelector('.quotation__icon');
                if ( (icon && detail) && (icon.classList && detail.classList)) {
                    icon.classList.toggle('is--toggle');
                    detail.classList.toggle('is--toggle');
                }
    
            })    
        })
    }

    toggleComponent();



}