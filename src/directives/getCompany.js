import { Emitter } from '../core/events.class.js';

export const getCompany = (element) => {
    setTimeout(() => {
        const elem = document.querySelectorAll(element)
        elem.forEach(element => {
            element.addEventListener('click', (e) => {
                const options = { data: e.target.text }
                const sendData = new Emitter('getcompany', 'body', options)
                removeList()
                sendData.emit()
            })
        })
    }, 300)
    return ''
}

const removeList = () => {
    const list = document.querySelector('.result__list');
    list.classList.add('is--hide')

    setTimeout(() => {
        list.innerHTML = ''
    }, 500)

}
