import { Emitter } from '../core/events.class.js';

export const onblur = (element) => { 
    setTimeout(() => {

        const elem = document.querySelector(element);

        elem.addEventListener('blur', (e) => {
           const options = {data: e.target.value};
           const onblur = new Emitter('onblur', '#search', options);
           onblur.emit();
        })     
   }, 300)
   return ''
}

document.querySelector('body').addEventListener('onblur', (e) => {
    if (!e.target.value)   e.target.classList.remove('has--value');
})