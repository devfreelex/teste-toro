import { Emitter } from '../core/events.class.js';

export const action = (element, eventName, status = null) => {
    setTimeout(() => {
     const actions = document.querySelectorAll(element);
     actions.forEach( action => {
         action.addEventListener('click', (e) => {
             const options = {
                 data: status
             }
             const action = new Emitter(eventName, element, options)
             action.emit()
         })
     })
    },300)
 }