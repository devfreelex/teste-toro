import {Emitter} from '../core/events.class.js';

export const keypress = (element) => {    
    setTimeout(() => {
         const elem = document.querySelector(element)

         elem.addEventListener('keyup', (e) => {
            const options = {data: e.target.value}
            const onkeypress = new Emitter('onkeypress', '#search', options)
            onkeypress.emit()
         })     
    }, 300)
    return ''
}

