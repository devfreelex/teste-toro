export const setClass = (element, strClass, condition) => { 
    if (condition) {
        setTimeout(()=> { 
           let elem = document.querySelector(`.${element}`);
           if (elem.classList) {
                elem.classList.add(`${strClass}`);
           }
        },50)
    }
    return '';
}