import headerComponent from '../components/header/header.component.js';
import searchComponent from '../components/search/search.component.js';
import resultComponent from '../components/search-result/result.component.js';
import modalComponent from '../components/modal/modal.component.js';
import loginComponent from '../components/login/login.component.js';

const mainModule = {
    headerComponent,
    searchComponent,
    resultComponent,
    modalComponent,
    loginComponent
};

export default mainModule