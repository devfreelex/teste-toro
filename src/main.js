import App from './core/app.class.js';
import Router from './core/router.class.js';

// MODULES
import mainModule from './modules/main.module.js.js';


const app = new App({
    modules:{
        mainModule
    }
})

app.init()